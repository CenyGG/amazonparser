import AmazonParser from './AmazonParser';
import { logDate } from '../utils'

export default class ParserService {
    constructor(urls = [], interval = 2) {
        this.urls = urls;
        this.interval = interval * 60 * 1000;
        this.parser = new AmazonParser();
        this.items = {};
        this.itemsInit = {};
        this.totalRequeests = 0;
        this.timeStart = new Date();
        this.failToUpdateUrls = new Set();
        this.failToUpdateInterval = 10 * 1000;
    };

    async start() {
        await this.work();
        this.refreshIntervalId = setInterval(() => { this.work() }, this.interval);
        this.refreshFailIntervalId = setInterval(() => { !!this.failToUpdateUrls.size && this.work_after_fail() }, this.failToUpdateInterval);
    };

    async parseUrl(url) {
        let { name, price, images } = await this.parser.parse(url);
        this.totalRequeests++;
        this.items[url] = { name, price, images };
        if (!this.itemsInit[url]) {
            let date = new Date();
            this.itemsInit[url] = { date, price };
        }
        console.log(JSON.stringify({ name, price, images }, null, 4));
        return !!name;
    }

    async work() {
        logDate()
        for (let url of this.urls) {
            let success = await this.parseUrl(url);
            !success && this.failToUpdateUrls.add(url)
        }
    }

    async work_after_fail() {
        logDate();
        for (let url of this.failToUpdateUrls) {
            let success = await this.parseUrl(url);
            success && this.failToUpdateUrls.delete(url);
        }
    }

    stop() {
        clearInterval(this.refreshIntervalId);
        clearInterval(this.refreshFailIntervalId);
    }

    add(url) {
        this.urls.push(url);
        this.parseUrl(url)
    };

    delete(url) {
        this.urls.splice(this.urls.indexOf(url), 1);
        this.failToUpdateUrls.delete(url);
    }
}