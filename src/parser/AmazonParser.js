let needle = require('needle');
var cheerio = require('cheerio');

class AmazonParser {
    parse(parseUrl) {
        var options = {
            cookies: this.cookies
        };

        function formatUrl(url) {
            if (url.startsWith("https://www"))
                return url;
            else if (url.startsWith("www"))
                return "https://" + url;
            else
                return "https://www." + url;
        }

        parseUrl = formatUrl(parseUrl)
        return needle('get', parseUrl, options)
            .then((res) => {
                let $ = cheerio.load(res.body);
                let price = [];
                let variationsCount = $('#variation_color_name>ul>li').length;
                if (variationsCount > 0) {
                    for (let i = 0; i < variationsCount; i++) {
                        let tmp = $(`#color_name_${i}_price`).text().trim();
                        !!tmp && price.push(tmp);
                    }
                }
                if (!price.length) {
                    let tmp = $('#priceblock_ourprice').text();
                    tmp && price.push(tmp);
                }
                if (!price.length) {
                    let dealPrice = $('#priceblock_dealprice').text().trim();
                    dealPrice && price.push("DEAL PRICE : " + dealPrice);
                }

                let imgTag = $('#landingImage');
                let images = [];
                let name;

                if (imgTag.length) {
                    let imagesJSON = JSON.parse(imgTag.attr('data-a-dynamic-image'));
                    for (let img in imagesJSON) {
                        !!img && images.push(img);
                    }
                    name = imgTag.attr('alt');
                }

                if ((!price.length || !images.length || !name) && $('title').text() === "Robot Check") {
                    console.log("!!! Robot Check !!! " + new Date());
                }
                return { name, price, images };
            })
            .catch(function (err) {
                console.log(err);
                return { name: '', price: '', images: [] }
            });
    }
}

if (!module.parent) {
    (async () => {
        console.log("Starting");
        var args = process.argv.slice(2);
        let parser = new AmazonParser();
        for (let arg of args) {
            console.log(JSON.stringify(await parser.parse(arg), null, 4));
        }
    })();
};

export default AmazonParser;