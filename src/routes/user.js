import express from 'express';
import bodyParser from 'body-parser'
import User from "../model/User"
import { ErrorType, Role } from "../utils"

let router = express.Router();

router.post('/singin', bodyParser.json(), function (req, res, next) {
  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    if (req.body.password !== req.body.passwordConf)
      return res.json({ success: false, info: ErrorType.wrong_second_password })

    let userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password
    }

    User.create(userData, (err, user) => {
      if (err) {
        return res.json({ success: false, info: ErrorType.create_user_error })
      } else {
        return res.json({ success: true })
      }
    });
  }
});


router.post('/login', bodyParser.json(), function (req, res) {
  if (req.body.email
    && req.body.password) {
    console.log("Login start")
    var userData = {
      email: req.body.email,
      password: req.body.password,
    }

    User.authenticate(userData, (err, user) => {
      if (err) {
        return res.json({ success: false, info: ErrorType.authentication_error })
      } else if (!user) {
        return res.send({ success: false })
      } else {
        req.session.userId = user._id
        req.session.name = user.name
        req.session.email = user.email
        req.session.isAdmin = !!user.role && (Role.admin === user.role)
        console.log(JSON.stringify(req.session, null, 4))
        return res.send({ success: true })
      }
    });
  }
});

router.get('/logout', bodyParser.json(), function (req, res) {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return res.send({ success: false, info: ErrorType.logout_error })
      } else {
        return res.send({ success: true })
      }
    });
  }
});


export { router };