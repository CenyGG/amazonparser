import express from 'express';
import { parserService } from '../services'
import { ErrorType } from "../utils"
import { adminMiddleware, authenticationMiddleware } from "./middlewares"

var router = express.Router();

router.use('/start', authenticationMiddleware, adminMiddleware)
router.use('/stop', authenticationMiddleware, adminMiddleware)

router.get('/start', function (req, res) {
    try {
        parserService.start();
    } catch (e) {
        console.log(e);
        return res.json({ success: false, info: ErrorType.start_error })
    }
    return res.json({ success: true })
});

router.get('/stop', function (req, res) {
    try {
        parserService.stop();
    } catch (e) {
        console.log(e);
        return res.json({ success: false, info: ErrorType.stop_error })
    }
    return res.json({ success: true })
});


export { router };