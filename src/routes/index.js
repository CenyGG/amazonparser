import { router as parserRoute } from './parser'
import { router as userRoute } from './user'
import { router as adminRoute } from './admin'

export { parserRoute, userRoute, adminRoute }