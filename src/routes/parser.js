import express from 'express';
import bodyParser from 'body-parser'
import { parserService } from '../services'
import { ErrorType } from "../utils"
import { authenticationMiddleware } from "./middlewares"

var router = express.Router();
router.use(authenticationMiddleware)

router.post('/addUrl', bodyParser.json(), function (req, res) {
    if (!req.body.url)
        return res.json({ success: false, info: ErrorType.bad_url })
    try {
        parserService.add(req.body.url)
    } catch (err) {
        console.log(err)
        return res.json({ success: false, info: ErrorType.parser_error })
    }
    return res.json({ success: true })
});

router.post('/deleteUrl', bodyParser.json(), function (req, res) {
    if (!req.body.url)
        return res.json({ success: false, info: ErrorType.bad_url })
    try {
        parserService.delete(req.body.url)
    } catch (err) {
        console.log(err)
        return res.json({ success: false, info: ErrorType.parser_error })
    }
    return res.json({ success: true })
});

export { router }