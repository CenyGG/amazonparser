import { ErrorType } from "../utils";


const authenticationMiddleware = (req, res, next) => {
    if (req.session && !!req.session.userId)
        return next()
    else
        return res.json({ success: false, info: ErrorType.not_authenticated })
}

const adminMiddleware = (req, res, next) => {
    if (req.session && !!req.session.userId && !!req.session.isAdmin)
        return next()
    else
        return res.json({ success: false, info: ErrorType.not_admin })
}

export { authenticationMiddleware, adminMiddleware }