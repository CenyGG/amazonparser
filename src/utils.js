export function logDate() {
    console.log([
        "\n************************************************************",
        new Date(),
        "************************************************************"].join('\n'));
};

export const ErrorType = {
    bad_url: "bad_url",
    start_error: "start_error",
    stop_error: "stop_error",
    parser_error: "parser_error",
    not_admin: "not_admin",
    not_authenticated: "not_authenticated",
    authentication_error: "authentication_error",
    create_user_error: "create_user_error",
    wrong_second_password: "wrong_second_password",
    logout_error: "logout_error"
}

export const Role = {
    admin: "admin"
}