import express from 'express'
import pasport from 'passport'
import session from 'express-session'
import mongoose from 'mongoose'

import * as config from './config';
import { parserRoute, userRoute, adminRoute } from './routes'
import { parserService } from './services'

mongoose.connect(config.MONGO_CONNECTION);

var app = express();

const port = process.env.PORT || 3000
app.use(express.static('static', { index: false, extensions: ['html'] }))
app.use(session({
  secret: "verySecret",
  resave: true,
  saveUninitialized: false,
  cookie: {
    secure: false
  }
}))

app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});

const urls = [
  "https://www.amazon.com/HP-Convertible-Touchscreen-Quad-Core-Bluetooth/dp/B0777GLYJ5/",
  "https://www.amazon.com/Fashioncraft-1950-Elephant-Bottle-Stopper/dp/B00DZ1IWYC/"
];
urls.forEach(parserService.add.bind(parserService));


// parserService.start();

app.use('/parser', parserRoute)
app.use('/user', userRoute)
app.use('/admin', adminRoute)


app.get('/*', function (req, res) {
  res.redirect('/index')
});